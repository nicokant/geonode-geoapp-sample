from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAdminUser, IsAuthenticated, IsAuthenticatedOrReadOnly  # noqa
from oauth2_provider.contrib.rest_framework import OAuth2Authentication


from .models import Poll
from .serializers import PollSerializer


class MapStoreResourceViewSet(viewsets.ModelViewSet):
    """ Only Authenticate User perform CRUD Operations on Respective Data
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication, OAuth2Authentication]
    permission_classes = [IsAuthenticatedOrReadOnly]
    model = Poll
    serializer_class = PollSerializer
