from .conf import settings
from six import string_types


class HookProxy(object):

    def __getattr__(self, attr):
        if not isinstance(settings.MAPSTORE2_ADAPTER_SERIALIZER, string_types):
            return getattr(settings.MAPSTORE2_ADAPTER_SERIALIZER, attr)
        else:
            import importlib
            cls = settings.MAPSTORE2_ADAPTER_SERIALIZER.split(".")
            module_name, class_name = (".".join(cls[:-1]), cls[-1])
            i = importlib.import_module(module_name)
            hook = getattr(i, class_name)()
            return getattr(hook, attr)


hookset = HookProxy()