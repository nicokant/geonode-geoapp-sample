from django.apps import AppConfig as BaseAppConfig
from django.utils.translation import ugettext_lazy as _


def run_setup_hooks(*args, **kwargs):
    from geonode.urls import urlpatterns
    from django.conf.urls import url, include

    urlpatterns += [
        url(r'^polls/', include('mapstore2_adapter.urls')),
        url(r'^', include('polls.geoapps.geopolls.api.urls')),
    ]


class AppConfig(BaseAppConfig):

    name = "polls"
    label = "polls"
    verbose_name = _("Polls")

    def ready(self):
        """Finalize setup"""
        run_setup_hooks()
        super(AppConfig, self).ready()