from django.apps import AppConfig as BaseAppConfig
from django.utils.translation import ugettext_lazy as _


class AppConfig(BaseAppConfig):

    name = "polls.geoapps"
    label = "polls_geoapps"
    verbose_name = _("Polls Geonode Apps Plugins")