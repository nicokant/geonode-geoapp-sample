from django.conf.urls import url, include

from geonode.api.urls import router

from . import views

router.register(r'geopolls', views.GeoPollViewSet)

urlpatterns = [
    url(r'^api/v2/', include(router.urls)),
]