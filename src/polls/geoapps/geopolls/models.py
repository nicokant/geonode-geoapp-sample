from django.db import models
from django.conf import settings
from django.db.models import signals
from django.utils.translation import ugettext_lazy as _

from geonode.geoapps.models import GeoApp
from geonode.base.models import resourcebase_post_save


class GeoPoll(GeoApp):

    app_type = models.CharField(
        _('%s Type' % settings.GEONODE_APPS_NAME),
        db_column='geopoll_app_type',
        default='GeoPoll',
        max_length=255)
    # The type of the current geoapp.


signals.post_save.connect(resourcebase_post_save, sender=GeoPoll)
