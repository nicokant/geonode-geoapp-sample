from geonode.geoapps import GeoNodeAppsConfig


class GeoPollsAppConfig(GeoNodeAppsConfig):
    name = 'polls.geoapps.geopolls'
    label = "geopolls"
    default_model = 'GeoPoll'
    verbose_name = "GeoNode App: GeoPoll"
    type = 'GEONODE_APP'
